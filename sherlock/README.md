# SHERLOCK

This repository contains the source code for the ITA project's SHERLOCK game for CE experiments. 
This version of the game supports the sherBot virtual player which can be found at [https://bitbucket.org/robbbh/sherlock_virtual_player](https://bitbucket.org/robbbh/sherlock_virtual_player). 
The original sherlock game can be found at [https://github.com/flyingsparx/sherlock](https://github.com/flyingsparx/sherlock).

The game uses the CENode library to maintain a knowledge base, which is described purely in ITA Controlled English.

For more information, take a look at the following webpages:

* [usukita.org](https://www.usukita.org)
* [cenode.io](http://cenode.io)

## Licensing

The contents of this repository are licensed under the Apache License v2. See LICENSE for further information.
